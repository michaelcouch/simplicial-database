import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="simplicialdatabase",
    version="0.0.1dev",
    author="Michael Couch",
    author_email="michael@michaelcouch.io",
    description="A data structure inspired by a CW complex",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/michaelcouch/simplicial-database",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache 2.0",
        "Operating System :: OS Independent",
    ],
)
